/*
 * fis
 * http://fis.baidu.com/
 */
'use strict';

var path = require("path");

function trimQuery(url){
    if (url.indexOf("?") !== -1) {
        url = url.slice(0, url.indexOf("?"));
    }
    return url;
}

module.exports = function(ret, conf, settings, opt){

    //
    // var startReg = /<!--\s*build:(\w+)(?:\(([^\)]+?)\))?\s+(\/?([^\s]+?))\s*-->/gim;
    // var endReg = /<!--\s*endbuild\s*-->/gim;

    //替换a标签和css或者js中的资源
    var tagReg = [/<a[^>]+href=([^>]+)>/gi,/<link[^>]+href=([^>]+)>/gi,/<script[^>]+src=([^>]+)>/gi,/<img[^>]+src=([^>]+)>/gi];

    //重组打包对象
    var map = {};
    fis.util.map(ret.src,function(subpath,file){

        if(!file.isHtmlLike && file.ext != ".tmpl"){

            map[subpath.replace("/src/","")] = file.getUrl(opt.hash, opt.domain);
        }
    });

    //concat
    fis.util.map(ret.src, function(subpath, file) {

        //html类文件，才需要做替换
        if (file.isHtmlLike) {
            var content = file.getContent();

            for(var i=0;i<tagReg.length;i++){

                content = content.replace(tagReg[i],function(tag,url){

                    if(url.indexOf("http://") > -1)return tag;

                    url = url.replace(/"|'/g,"");

                    url = url.split(" ")[0];

                    //url = url.replace("../","");

                    // var pkg = map[url.replace("../","")]; // 只替换掉一个../
                    var pkg = map[url.replace(/\.\.\//g, "")]; // 全局替换../

                    if(pkg){

                        return tag.replace(url,pkg);
                    }
                    return tag;
                });
            }

            file.setContent(content);
        }
    });
};