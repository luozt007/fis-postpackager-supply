> 网易前端大神[gzzhengxuze]开发的插件，npmjs地址：<https://www.npmjs.com/package/fis-postprocessor-supply>

原项目有个小问题，就是对于超过1层的深层级目录的资源就不会替换，本项目把此bug修复并提交了issue给原作者了，当原作者修复问题后，本项目将自动下架。

改动地方: index.js

```javascript
// var pkg = map[url.replace("../","")]; // 只替换掉一个../
var pkg = map[url.replace(/\.\.\//g, "")]; // 全局替换../
```


### 用于补充前端html页面中打包问题，目前补充a标签

>   属于Fis的插件

```html
	//在html页面中
	//源代码
    <a href="images/aa.jpg">我是一个资源</a>
    //编译后，可能是这样
    <a href="http://test.nie.163.com/zgmh/images/aa_09d76s.jpg">我是一个资源</a>
```

```javascript
    //配置
    postpackager : 'supply'
```
